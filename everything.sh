# Based on https://github.com/frdmn/minebackup.sh/blob/master/minebackup.sh

#!/bin/bash

#Setup
#JAR file
JAR="forge-1.12.2-14.23.2.2651-universal.jar"
#Short timestamp
SSTAMP=`date +%d-%m-%Y`
#Long timestamp
LSTAMP=`date +%d-%m-%Y_%H%M%S`
#Name of compressed file
BFILE="world.$SSTAMP.tar.gz"
#call for rdiff
BIN_RDIFF="rdiff-backup"
#call for tar
BIN_TAR="tar"
#call for nice
BIN_NICE="nice"
#call for ionice
BIN_IONICE="ionice"
#Name of directory for rdiff-backup
BDIR="world.$SSTAMP"
#Path to world file
WPATH="/home/alex/mc/custom/1-12/amalgam-8/world"
#Path to rdiff-backup directory (will be created if not already)
SPATH="/mnt/HDD/MCBackups/amalgam-8"
BPATH="/mnt/HDD/MCBackups/amalgam-8/${BDIR}"
#Mux Setup
MUX="tmux send-keys -t mc"
# nice and ionice settings
RUNBACKUP_NICE="${BIN_NICE} -n19"
RUNBACKUP_IONICE="${BIN_IONICE} -c 3"
#echo "Zipped file to be saved as $BFILE"

function is_running() {
  if ps aux | grep -v grep | grep $JAR >/dev/null 2>&1
    then
  		PID=$(pgrep -f $JAR)
  		TIME=$(ps -p $PID -o etimes=)
  		if (( ${TIME} > 299 ))
  			then
  				echo 1
  			else
  				echo 0
  		fi
    else
      echo 0
  fi
}

function is_running_long {
  if ps aux | grep -v grep | grep $JAR >/dev/null 2>&1
    then
      PID=$(pgrep -f $JAR)
      TIME=$(ps -p $PID -o etimes=)
      if (( ${TIME} > 71999 ))
        then
          echo 1
        else
          echo 0
      fi
    else
      echo 0
  fi
}

STAT=$(is_running)
STAT2=$(is_running_long)

function restart {
  if [ $STAT2 -eq 1 ]
    then
      #give warnings
      echo "[$LSTAMP:restart] About to restart the server"
      ${MUX} "say I hope this isn't too inconvient for you but" C-m
      sleep 1
      ${MUX} "say this ole girl has been running for a while now" C-m
      sleep 1
      ${MUX} "say so I'm going to restart her in 5 minutes" C-m
      sleep 270
      ${MUX} "say 30 seconds till restart" C-m
      ${MUX} "say It should take less than 3 minutes to restart" C-m
      sleep 20
      ${MUX} "say 10 seconds till restart" C-m
      sleep 8
      ${MUX} "say Here goes nothing..." C-m
      sleep 2
      #stop server
      ${MUX} "stop" C-m
      #restart server
	  sleep 10
      ${MUX} "./start.sh" C-m
      echo "[$LSTAMP:restart] The server should have been restarted"
  else
    echo "[$LSTAMP:restart] The server did not need to be restarted"
  fi
}

function frestart {
  if [ $STAT -eq 1 ]
    then
      #give warnings
      echo "[$LSTAMP:restart] About to restart the server"
      ${MUX} "say Unplanned restart in 1 minute" C-m
      sleep 50
      ${MUX} "say 10 seconds till restart" C-m
	  sleep 10
      #stop server
      ${MUX} "stop" C-m
	  sleep 10
      #restart server
      ${MUX} "./start.sh" C-m
      echo "[$LSTAMP:restart] The server should have been restarted"
  else
    echo "[$LSTAMP:restart] The server did not need to be restarted"
  fi
}

function bCleanup() {
echo "this has not been implemented yet"
}

# Function to chekc if server is running and run rdiff-backup
function rdiff() {
if [ $STAT -eq 1 ]
	then
		echo "[$LSTAMP:smartrdiff] About to run smart rdiff, determing if directory exists"
		if [ ! -d "${BPATH}" ]
		then
			echo "Making Directory..."
			mkdir $SPATH
			chmod 777 $SPATH
			mkdir ${BPATH}
			chmod 777 ${BPATH}
			${RUNBACKUP_NICE} ${RUNBACKUP_IONICE} ${BIN_RDIFF} ${WPATH} ${BPATH}
		else
			echo "Directory exists, proceeding..."
			${RUNBACKUP_NICE} ${RUNBACKUP_IONICE} ${BIN_RDIFF} ${WPATH} ${BPATH}
		fi
		echo "[$LSTAMP:smartrdiff] Smart rdiff complete"
	else
		echo "[$LSTAMP:smartrdiff] Smart rdiif aborted: The server was either not running or ot running long enough"
fi
}

# Function to check if server is running and disable in-game saving
function save_off() {
if [ $STAT -eq 1 ]
	then
		echo "[$LSTAMP:smartsaveoff] About to tell server about saveoff"
		${MUX} "say World backup in 10 seconds..." C-m
		sleep 10
		${MUX} "say Starting world backup..." C-m
		${MUX} "save-off" C-m
		${MUX} "save-all" C-m
		echo "[$LSTAMP:smartsaveoff] Told server about saveoff"
	else
		echo "[$LSTAMP:smartsaveoff] Smart saveoff aborted: The server was either not running or ot running long enough"
fi
}

# Function to check if server is running and enable in-game saving
function save_on() {
if [ $STAT -eq 1 ]
	then
		echo "[$LSTAMP:smartsaveon] About to tell server about saveon"
		${MUX} "say Finished world backup..." C-m
		${MUX} "save-on" C-m
		echo "[$LSTAMP:smartsaveon] Told server about saveon"
	else
		echo "[$LSTAMP:smartsaveon] Smart smartsaveon aborted: The server was either not running or ot running long enough"
fi
}

#Find out what to do
case "${1}" in
	bCleanup)
		bCleanup
	;;
	backup)
		save_off
		rdiff
		save_on
	;;
	restart)
		restart  
	;;
	frestart)
		frestart
	;;
  *)cat << EOHELP
Usage: ${0} COMMAND [ARGUMENT]

COMMANDS
bCleanup    	Backs up entire server folder
backup		  	Starts smart rdiff (checks to see how long server is running before backup)
restart			Restarts if running more than 20 hours
frestart		Forces restart if running more than 5 minutes
EOHELP
    exit 1
  ;;
esac

exit 0
