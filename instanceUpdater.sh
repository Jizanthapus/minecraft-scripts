#!/bin/bash

# Get instance info
INSTANCE_INFO_PATH="/home/alex/mc/instanceInfo.sh"
if [[ -f $INSTANCE_INFO_PATH ]]; then
  source $INSTANCE_INFO_PATH
else
  echo "No access to instance info file!"
  exit 1
fi
# Setup
OLD_INSTANCE_VERSION=$(expr $INSTANCE_VERSION - 1)
OLD_INSTANCE_PATH=$INSTANCE_ROOT/$INSTANCE_NAME_BASE-$OLD_INSTANCE_VERSION
FORGE_URL="https://files.minecraftforge.net/maven/net/minecraftforge/forge/${FORGE_VERSION}/forge-${FORGE_VERSION}-installer.jar"
FORGE_INSTALLER="$(basename $FORGE_URL)"

# Check to see if INFO version is different from existing version
echo "Looking at instances in:"
echo -e "\t$INSTANCE_ROOT"
cd $INSTANCE_ROOT
for d in */ ; do
  d=$(echo $d | sed 's/\///')
  IFS='-' read -r -a array <<< "$d"
  if [[ "${array[0]}" = $INSTANCE_NAME_BASE ]]; then
     if [[ "${array[1]}" -gt $INSTANCE_VERSION ]]; then
       echo "Update not needed. Newer version already in place."
       exit 0
     elif [[ "${array[1]}" = $INSTANCE_VERSION ]]; then
       echo "Update not needed. Update already in place."
       exit 0
     else
       echo "Only old versions present."
       echo "Proceeding with update..."
     fi
  fi
done

# Make directory for new version
echo "Making directory for $INSTANCE_NAME..."
mkdir $INSTANCE_PATH
cd $INSTANCE_PATH

# Create temp directory for the update download and retrieve it
echo "Making temp directory for zip download..."
mkdir updateZip
cd updateZip
ZIP_NAME="$INSTANCE_NAME.zip"
echo "New instance URL: $INSTANCE_URL"
echo "Downloading new instance as $ZIP_NAME..."
wget -q --show-progress $INSTANCE_URL -O $ZIP_NAME

# Extract the new stuff
echo "Ectracting files from new instance..."
unzip -qq $ZIP_NAME $INSTANCE_NAME/scripts/* $INSTANCE_NAME/mods/* $INSTANCE_NAME/config/*
mv $INSTANCE_NAME/mods .
mv $INSTANCE_NAME/config .
mv $INSTANCE_NAME/scripts .
rm -r $INSTANCE_NAME
rm $ZIP_NAME
cd $INSTANCE_PATH

# Download new forge if needed
OLD_FORGE_PATH="$(find $OLD_INSTANCE_PATH -name forge-*-universal.jar)"
echo "Considering old Forge found at:"
echo -e "\t$OLD_FORGE_PATH"
OLD_FORGE_VERSION="$(basename $OLD_FORGE_PATH)"
OLD_FORGE_VERSION=${OLD_FORGE_VERSION//forge-}
OLD_FORGE_VERSION=${OLD_FORGE_VERSION//-universal.jar}
echo "Old Forge version found as $OLD_FORGE_VERSION"
if [[ $OLD_FORGE_VERSION = $FORGE_VERSION ]]; then
  echo "New instance uses same version. Forge does not need to be updated."
  FORGE_UPDATE=0
else
  echo "New instance uses $FORGE_VERSION. Forge needs to be updated."
  FORGE_UPDATE=1
fi
if [[ $FORGE_UPDATE ]]; then
  echo "Downloading new Forge version..."
  wget -q --show-progress $FORGE_URL -O $FORGE_INSTALLER
  echo "Installing new Forge version..."
  java -jar $FORGE_INSTALLER --installServer >/dev/null 2>&1
  rm *-installer.jar
  rm *-installer.jar.log
fi

# Stop server if running and copy world folder
if ps aux | grep -v grep | grep "forge" >/dev/null 2>&1
  then
    echo "Server is running. Shutting it down..."
    # Alert server about shutdown
    tmux send-keys -t ${TMUX_NAME:-"mc"}:0 "say Server shutting down for update in 30 seconds..." Enter
    sleep 10
    echo "Shutting down in 20 seconds"
    sleep 10
    echo "Shutting down in 10 seconds"
    sleep 10
    tmux send-keys -t ${TMUX_NAME:-"mc"}:0 "stop" Enter
    sleep 10
  else
    echo "Server not running. Proceeding with instance copy..."
fi
echo "Move files from old version at:"
echo -e "\t$OLD_INSTANCE_PATH"
rsync -aq $OLD_INSTANCE_PATH/ $INSTANCE_PATH --exclude crash-reports/ --exclude logs/ --exclude libraries/ --exclude forge-*-universal.jar --exclude world/ --exclude mods/ --exclude config/ --exclude scripts/
if ! [[ $FORGE_UPDATE ]]; then
  echo "Moving old forge version..."
  rsync -aq $OLD_INSTANCE_PATH/forge-*-universal.jar $OLD_INSTANCE_PATH/libraries/ $INSTANCE_PATH
fi

# Move new stuff out of temp directory
mv updateZip/* .
rm -r updateZip
