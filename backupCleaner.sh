# Delete folders older than 90 days

# Setup
#Backup Directory
BPATH="/mnt/HDD/MCBackups/KV-Pack-2-0/"
#Days to keep backups
BDAYS="7"

find $BPATH* -maxdepth 0 -type d -mtime +$BDAYS | xargs rm -rf
