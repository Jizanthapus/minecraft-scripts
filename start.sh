#!/bin/bash

# Get info from instance file
source /home/alex/mc/instanceInfo.sh

# Variables
WARN="Version not given"
JAR="forge-${FORGE_VERSION:-$WARN}-universal.jar"
JAR_PATH="$INSTANCE_PATH/$JAR"
CPU_COUNT="2"
OPTIONS="nogui"
DEFAULT_RAM_MIN="1G"
DEFAULT_RAM_MAX="3G"
JAVA="java -Xmx${RAM_MAX:-$DEFAULT_RAM_MAX} -Xms${RAM_MIN:-$DEFAULT_RAM_MIN} -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=${CPU_COUNT} -XX:+AggressiveOpts -jar ${JAR_PATH} ${OPTIONS}"



if [ -e ${JAR_PATH} ]; then
	tmux new-session -A -d -s ${TMUX_NAME:-"mc"}
	sleep 0.5
	tmux send-keys -t ${TMUX_NAME:-"mc"}:0 "cd $INSTANCE_PATH" Enter
	tmux send-keys -t ${TMUX_NAME:-"mc"}:0 "$JAVA" Enter
else
	echo "Forge jar does not exist!"
	echo "Tried to find ${JAR}"
fi
